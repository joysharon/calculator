import {StyleSheet, View, Text, FlatList, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';

export default function App() {
  const [result, setResult] = useState(false);
  const [string, setString] = useState('');
  const [ans, setAns] = useState('');
  const [dummy, setDummy] = useState(false);
  let regex = /[*+-/%]/;

  const numbers = [
    {name: 'C', key: '17'},
    {name: '%', key: '18'},
    {name: '\u232B', key: '19'},
    {name: '/', key: '20'},
    {name: '7', key: '1'},
    {name: '8', key: '2'},
    {name: '9', key: '3'},
    {name: '*', key: '4'},
    {name: '4', key: '5'},
    {name: '5', key: '6'},
    {name: '6', key: '7'},
    {name: '-', key: '8'},
    {name: '1', key: '9'},
    {name: '2', key: '10'},
    {name: '3', key: '11'},
    {name: '+', key: '16'},
    {name: '00', key: '12'},
    {name: '0', key: '13'},
    {name: '.', key: '14'},
    {name: '=', key: '15'},
  ];
  const [history, setHistory] = useState(['hii']);
  const operators = ['+', '-', '*', '/'];

  const pressHandler = key => {
    console.log(history);
    //console.log(string)
    if (
      operators.includes(string.slice(-1)) &&
      (key == '+' || key == '-' || key == '*' || key == '/') &&
      result == false
    ) {
      console.log(string);
      setString(string.slice(0, string.length - 1));
    }
    if (key == '=') {
      if (string == '') {
        return setString('');
      }
      try {
        setString(eval(string).toString());
        setResult(true);

        setHistory(prevHistory => {
          console.log(history);
          // console.log(prevHistory)

          // if(prevHistory.length==10){
          //   prevHistory.shift()
          // }
          if (regex.test(string)) {
            prevHistory.push(string);
          }
          // storeData()
          return prevHistory;
        });
      } catch {
        setString('SYNTAX ERROR!');
        setResult(true);
      }
    } else if (key == '\u232B') {
      setString(string.slice(0, string.length - 1));
      setAns(string.slice(0, string.length - 1));
    } else if (key == 'C') {
      setString('');
      setAns('');
    } else {
      setString(prevState => {
        if (result == true) {
          //setString("")
          setResult(false);
          if (key == '+' || key == '-' || key == '*' || key == '/') {
            return prevState.concat(key);
          }
          return setString(key);
        }
        try {
          setAns(eval(prevState.concat(key)));
          setDummy(prevState.concat(key));
        } catch {
          setAns(prevState.concat(key));
        }

        return prevState.concat(key);
      });
    }
  };

  return (
    <View style={styles.input}>
      <Text>App</Text>
      <View style={styles.result}>
        <Text style={styles.answer}>{string}</Text>
        <Text style={styles.mainAns}>{ans}</Text>
      </View>
      <View style={styles.history}>
        <FlatList
          horizontal
          data={history}
          renderItem={({item}) => (
            <Text style={styles.textHistory}>{item}</Text>
          )}
        />
      </View>
      <View style={styles.numbers}>
        <FlatList
          keyExtractor={item => item.key}
          numColumns={4}
          data={numbers}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => pressHandler(item.name)}>
              <Text style={styles.item}>{item.name}</Text>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    // borderColor:'#777',
    // padding:8,
    // marginTop:30,
    // width:200
    flex: 1,
    // backgroundColor:"pink"
  },
  item: {
    marginTop: 15,
    padding: 10,
    color: 'black',
    fontSize: 24,
    marginHorizontal: 30,
  },
  numbers: {
    flex: 1,
    marginTop: 180,
    padding: 15,
    // backgroundColor:`#f0f8ff`,
  },
  result: {
    marginTop: 40,
    alignSelf: 'flex-end',
  },
  answer: {
    fontSize: 20,
  },
  mainAns: {
    alignSelf: 'flex-end',
  },
  history: {
    marginTop: 180,
    position: 'absolute',
    padding: 10,
  },
  textHistory: {
    marginTop: 15,
    padding: 10,
    color: 'black',
    fontSize: 24,
    marginHorizontal: 15,
    backgroundColor: `#f0f8ff`,
  },
});
